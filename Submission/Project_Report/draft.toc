\select@language {english}
\contentsline {chapter}{\numberline {1}Project Homepage}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Problem Statement and Goals}{2}{chapter.2}
\contentsline {chapter}{\numberline {3}Prior and Related Work}{3}{chapter.3}
\contentsline {chapter}{\numberline {4}Project Formulation, Setup and Methodology}{4}{chapter.4}
\contentsline {section}{\numberline {4.1}IMORL framework}{4}{section.4.1}
\contentsline {section}{\numberline {4.2}Histogram of Oriented Gradients}{6}{section.4.2}
\contentsline {section}{\numberline {4.3}Support Vector Machine}{6}{section.4.3}
\contentsline {section}{\numberline {4.4}Decision Trees}{7}{section.4.4}
\contentsline {section}{\numberline {4.5}k-Nearest Neighbor}{7}{section.4.5}
\contentsline {section}{\numberline {4.6}Methodology}{8}{section.4.6}
\contentsline {chapter}{\numberline {5}Implementation}{10}{chapter.5}
\contentsline {section}{\numberline {5.1}Database}{10}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Video Database}{10}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Still Images database}{11}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}IMORL Implementation}{11}{section.5.2}
\contentsline {section}{\numberline {5.3}HOG-SVM/DT/k-NN}{12}{section.5.3}
\contentsline {chapter}{\numberline {6}Results and Interpretation}{13}{chapter.6}
\contentsline {chapter}{\numberline {7}Description of tried and used algorithms}{16}{chapter.7}
\contentsline {chapter}{\numberline {8}Conclusions}{17}{chapter.8}
