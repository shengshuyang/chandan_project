%% featExtract
% I = cell array of images
% classLabel = label for the individual class
% X_feature = features extracted of dimensions
% Code written by Chetan Sadhu and Chandan Basavaraju

%%
function X_feature = featExtract(I,classLabel)

X_feature = [];
for i=1:size(I,1)
    X_feature = cat(1,X_feature,hog_feature_vector(I{i}));
end

if (length(classLabel) > 1)
    C = repmat(classLabel,[size(X_feature,1),1]);
    X_feature = [X_feature C];
else
    X_feature(1:end,end+1) = classLabel;
end

end