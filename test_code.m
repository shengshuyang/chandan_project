clc
clear all
close all

% im = imread('D:\EE660\Project\EE660\ee660\VideoDB\Video6.mp4');
%
%  figure(1);
%  imshow(im); title('Original Image');
%

vid = VideoReader('Video Database\Minions.mp4');
numFrames = vid.NumberOfFrames;
for i = 1:numFrames
    flag = 0;
    if ( i<160 || i>170 && i<185 || i>190 && i<618 || i>800 && i<1100 )
        im = read(vid,i);
        
        Igray = rgb2gray(im);
        % figure(2);
        % imshow(Igray);
        % Igray = im;
        %%Convert to binary
        Ibw = im2bw(Igray, 0.6);
        %invert the image
        Ibw = ~Ibw;
        
        %Morphology
        Ifill = imfill(Ibw,'holes');
        
        %blob analysis
        [Ilabel,num] = bwlabel(Ifill);
        %  disp(num);
        
        Iprops = regionprops(Ilabel);
        Ibox = [Iprops.BoundingBox];
        
        %reshape
        Ibox = reshape(Ibox,[4 num]);
        % figure(3);
        % imshow(Ifill);
        
        
        %         for cnt = 1:num
        %             rectangle('Position', Ibox(:,cnt),'EdgeColor', 'r');
        %         end
        %
        %         hold off;
        %
        
        k=1;
        while(k<=num)
            Ar(k) = Iprops(k).Area;
%             if (Ar(k) < 18000)
%                 flag = 1;
%                 break;
%             end
            k=k+1;
        end
        
%         if (flag == 1)
%             flag = 0;
%             continue;
%         end
        
        n=Ar>18000;
        
        c=1;z=1;
        while(c<=num)
            if n(c) == 1;
                index(z) = c ; z=z+1;
            end
            c=c+1;
        end
        j=1;
        t = length(index);
        
        while(j<=t)
            Image{i,j} = imcrop(Igray, Ibox(:,index(j))');
            j=j+1;
        end
        %imshow(Image{1,1});
        %         if (Image{i,j})
        %         end
    end
end
%%
k = 1;
l = 1;
for i=1:size(Image,1)
    if (size(Image{i,1},1) > 200 && size(Image{i,1},2) > 150 && size(Image{i,1},1) < 300 && size(Image{i,1},2) < 300) 
        I{k,1} = Image{i,1};
        I{k,2} = 1;
        k = k + 1;
    end
    if (size(Image{i,2},1) > 200 && size(Image{i,2},2) > 150)
        I{k,1} = Image{i,2};
        I{k,2} = 2;
        k = k + 1;
    end
end
%%
save('preProcessing.mat','I');