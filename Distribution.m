%% updating distribution function
function [P_final Et] = Distribution(P_t,Prediction_label,Test_mat, nBins, nChunks)

% Prediction = sign(new_Train(:,1:nBins)*w+bias);
% [predicted_label, accuracy, decision_values] = svmpredict(Test_mat(1:50,nBins+1), Test_mat(1:50,1:nBins), model);

diff = abs(Prediction_label-Test_mat);


et = sum(P_t(diff~=0));
Et = sum(P_t(diff==0));
P_update = zeros(1,nChunks);
for i=1:nChunks
    if diff == 0
        P_update(i) =P_t(i)*et;
    else
        P_update(i) = P_t(i);
    end
end

normalize = sum(P_update);
P_final = P_update/normalize;
end