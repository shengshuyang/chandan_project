%% This is a function to select the template.
% I returns the template image.
% vid returns the video object created to access the webcam
function [I,vid] = TempSelect
%% Input from webcam
% Object creation
% winvideo is the adapter name
% 1 is the device ID
% MJPG_640x480 specifies the colorspace and resolution of the image
vid = videoinput('winvideo', 1, 'MJPG_320x240');
vid.FramesPerTrigger = 1;
% Convert the video to grayscale
vid.ReturnedColorspace = 'grayscale';
% Start recording or aquisition
start(vid);
% Open the preview window
preview(vid);
% Take the snapshot of the current frame
img = (getsnapshot(vid));
% Display that image
imshow(img);
% Select the number of objects to be detected in the scene
prompt = {'Enter number of objects to be detected:'};
N = inputdlg(prompt,'Input');
%% Region Selection or Template selection
% Create a draggable rectangle for object 1
I = cell(N);
for i=1:N
    h1 = imrect;
    % Gives the position of the rectangle in the image for object 1
    pos1 = h1.getPosition();
    % crop the image to the position specified by the rectangle to get object 1
    I{i} = imcrop(img,pos1);
end
% Combine 2 images to make a single template 
% % I = appendimages(I);
% Apply Histogram equalization to the resultant template
% % I = histeq(I);